A demonstration of D3’s vertically-oriented [bullet charts](https://github.com/d3/d3-plugins/tree/master/bullet), based on [this example](http://bl.ocks.org/mbostock/4061961).
